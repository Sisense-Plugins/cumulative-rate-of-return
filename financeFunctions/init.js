//	Initialize the copyWidget object
prism.run([function() {

	//	Define the menu label
	var headerMenuCaption = "Financial Functions",
		propName = 'financeFunctions',
		hidePropName = 'hideValue';

	//	What chart types are supported?
	var supportedChartTypes = ["chart/bar", "chart/column", "chart/line", "chart/area"];

	//	default settings object	
	var defaultSettings = {};

	//////////////////////////////////
	//	Utility Functions			//
	//////////////////////////////////

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//  Function to make sure this is a settings menu for a measure
    function isSettingsMenu(args,location) {
        
        //  Try to evaluate
        try {

        	//	Must be in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate === "widget");
			if (!widgetEditorOpen){
				return false
			}

            // Widget must be an allowed chart            
            if(!widgetIsSupported(prism.$ngscope.widget.type)){
                return false;
            }

            if (location == "metadataMenu") {

	            //	Make sure the user clicked on a metadata item menu
	            var isMetadataItemMenu = (args.settings.name === "widget-metadataitem");
	            if (!isMetadataItemMenu){
	            	return false;
	            }

	        } else if (location == "widgetMenu"){

	        	//	Make sure the user clicked on a widget menu
	            var isWidgetMenu = (args.ui.css == "w-menu-host");
	            if (!isWidgetMenu){
	            	return false;
	            }
	        }

        } catch(e) {
            return false;
        }

        return true;
    }

    //	Function to add an event handler
    function addHandler(widget,event,handler){
        if (!hasEventHandler(widget,event,handler)){
            widget.on(event, handler);
        }
    }

    //  Function to check if model contains certain event handler for specified event name
    function hasEventHandler(model, eventName, handler) {

        return model.$$eventHandlers(eventName).indexOf(handler) >= 0;
    }

    //	Function handler for when hiding a data series
    function hideValueClicked(){

    	//	Update the item's property
    	this.item[hidePropName] = this.item[hidePropName] ? !this.item[hidePropName] : true;

    	//	redraw the widget
    	this.widget.redraw();
    }

    //	Event handler for when the menu item was clicked
    function menuItemClicked(){

    	//	Get a reference to the widget's settings
    	var settings = this.widget.options[propName];

    	//	Check for any existing settings for this function
    	var funcSettings = $$get(settings, this.functionObj.label, null);
    	if (funcSettings) {
    		//	Existing settings found for this financial function
    		var existingSetting = $$get(funcSettings, this.caption, false);

    		//	Init/Flip the existing setting
    		settings[this.functionObj.label][this.caption] = !existingSetting;

    	} else {
    		//	Init brand new settings (all financial functions)
    		settings = {};
    		settings[this.functionObj.label] = {};
    		settings[this.functionObj.label][this.caption] = true;
    	}

    	//	Save settings back to widget
    	this.widget.options[propName] = settings;

		//	Redraw the widget
		this.widget.redraw();
	}

	//	Event handler for when the metadata item menu was clicked
	function metadataItemClicked(){

    	//	Get a reference to the widget's settings
    	var settings = this.item[propName],
    		label = this.functionObj.label,
    		caption = this.caption;

    	//	Check for no settings defined at all
    	if (typeof settings === "undefined"){
    		settings = {};
    	}

    	//	Check for no settings defined for this function
    	if (typeof settings[label] === "undefined"){
    		settings[label] = {};
    	}

    	//	Check all other items, and make sure any other items don't have this attribute set
    	this.panelItems.forEach(function(item){

    		//	Check for an existing property
    		var path = propName + '.' + label + '.' + caption;
    		var existing = $$get(item, path, false);
    		if (existing){

    			//	Found another metadata item with the same attribute, delete it
    			delete item[propName][label][caption];
    		}
    	})

    	//	Flip/Init settings for this metadata item
    	var existing = $$get(settings[label], caption, false);

    	//	Clear any old settings for this function
    	settings[label] = {}
    	settings[label][caption] = !existing;

    	//	Update the settings of the item
    	this.item[propName] = settings;

		//	Redraw the widget
		this.widget.redraw();
	}

    //	Function to add the widget menu item
    function addMenuItem(event, args){

    	//	Make sure we can add in the new menu item
		var widgetMenu = isSettingsMenu(args,'widgetMenu');
		if (widgetMenu){
			addWidgetMenuItem(event,args);
			return;
		}

		//	Check for metadata menu
		var metadataMenu = isSettingsMenu(args,'metadataMenu');
		if (metadataMenu) {
			addMetadataMenuItem(event, args);
			return;
		}
    }

    //	Function to add the widget menu item
    function addWidgetMenuItem(event,args){

    	//	Get the widget object
		var widget = event.currentScope.widget;

		//	Get saved settings
		var settings = widget.options[propName];
		if (typeof settings === 'undefined'){
			//	Create defaults, if not defined
			settings = defaultSettings;
		}

		//  Add the header for the financial functions
        var header = {
        	caption: headerMenuCaption,
            type: "header"
        };
		args.settings.items.push(header);	

		//	Loop through each available function
		myFunctions.forEach(function(func){

			//	Make sure the function object is valid
			var isValid = (func.data && func.results);
			if (isValid){

				//	Create the base menu item
				var menuItem = {
					'caption': func.label,
					'items':[] 
				}

				//	Check to see if any settings are defined for this function
				var functionSettings = $$get(settings, func.label, {});

				//	Loop through each possible result
				func.results.forEach(function(result){

					//	create a sub menu item
					var subMenuItem = {
						caption: result.label,
						checked: functionSettings[result.label],
						closing: false,
						disabled: false,
						size: "xl",
						type: "check",
						widget : widget,
						widgetSettings : settings,
						functionObj: func,
						functionSettings: functionSettings,
						execute: menuItemClicked
					}

					//	Add to menu item
					menuItem.items.push(subMenuItem);
				})

				//	Add menu item
				args.settings.items.push(menuItem);

			}
		})
    }

    //	Function to add the metadata menu item
    function addMetadataMenuItem(event,args){

		//	Get the widget object
		var widget = event.currentScope.widget;

		//	Get the metadata item that was clicked
		var item = args.settings.item;

		//	Get saved settings
		var settings = item[propName];
		if (typeof settings === 'undefined'){
			//	Create defaults, if not defined
			settings = defaultSettings;
		}

		//  Create a separator
        var separator = {
            type: "separator"
        };

        //	Create the base menu item
		var hidden = {
			caption: 'Hide Value',
			checked: item[hidePropName],
			closing: false,
			disabled: false,
			size: "xl",
			type: "check",
			item: item,
			widget: widget,
			execute: hideValueClicked
		}

		//	Create settings menu
		var options = {
			caption: headerMenuCaption,
			items:[]
		};

		//	Loop through each available function
		myFunctions.forEach(function(func){

			//	Make sure the function object is valid
			var isValid = (func.data && func.results);
			if (isValid){

				//	Create the base menu item
				var header = {
					'caption': func.label,
					'type':'header' 
				}

				//	Add header item
				options.items.push(header);

				//	Check to see if any settings are defined for this function
				var functionSettings = $$get(settings, func.label, {});

				//	Loop through each possible result
				func.data.forEach(function(result){

					//	create a sub menu item
					var subMenuItem = {
						caption: result.label,
						checked: functionSettings[result.label],
						closing: false,
						disabled: false,
						size: "xl",
						type: "check",
						widget : widget,
						functionObj: func,
						item: item,
						panelItems: widget.metadata.panel('values').items,
						execute: metadataItemClicked
					}

					//	Add to menu item
					options.items.push(subMenuItem);
				})

				

			}
		})
		
		//	Add options to the menu
		args.settings.items.push(separator);
		args.settings.items.push(hidden);
		args.settings.items.push(options);	
    }

    //	Function to calculate a new color
    function calcColor(hex, offset){   

    	//	Calculate the luminance to use
    	var offsetFactor = -0.25,
    		lum = isNaN(offset) ? 0 : ( (offset % 2) == 1 ? (offset * offsetFactor) : (offset * offsetFactor * -1));

    	// validate hex string
		hex = String(hex).replace(/[^0-9a-f]/gi, '');
		if (hex.length < 6) {
			hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
		}

		// convert to decimal and change luminosity
		var rgb = "#", c, i;
		for (i = 0; i < 3; i++) {
			c = parseInt(hex.substr(i*2,2), 16);
			c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
			rgb += ("00"+c).substr(c.length);
		}

		return rgb;
    }

	//////////////////////////////////
	//	Business Logic				//
	//////////////////////////////////

	function runCalculations(widget, func, settings){

		//	Init objects
		var results = [],
			data = {},
			items = widget.metadata.panel('values').items;

		//	Get the data
		func.data.forEach(function(column){

			//	Find a matching metadata item
			var match = $.grep(items, function(i){
				
				//	Look for settings
				var settingsPath = propName + '.' + func.label + '.' + column.label,
					isSet = $$get(i,settingsPath,false);

				return isSet;
			})[0];

			if (match){

				//	Find the metadata item's highcharts series
				var dataSeries = $.grep(widget.queryResult.series, function(s){
					return s.name === match.jaql.title;
				})[0];

				//	If found, save to the data object
				if (dataSeries){
					data[column.label] = dataSeries;
				}
			}
		})

		//	Run each function
		func.results.forEach(function(myFunction){

			var settingPath = propName + '.' + func.label + '.' + myFunction.label,
				shouldRun = $$get(widget.options, settingPath, null);
			if (shouldRun) {

				//	Get a new data series
				var newSeries = myFunction.calculation(data,calcColor);

				//	Save to results
				results.push(newSeries);
			}
		})

		return results;
	}

	//	Function to get the midpoint
	function checkForFunctions(widget,args) {

		//	Init an array to hold any new functions
		var seriesToAdd = [];

		//	Loop through any functions added to this chart
		var functions = $$get(widget.options, propName, {});
		for (func in functions){

			//	Make sure this object exists
			if (functions.hasOwnProperty(func)){

				//	Get a reference to the function definitino
				var def = $.grep(myFunctions, function(w){
					return w.label == func;
				})[0]

				//	Add 1+ new series to list
				var newSeries = runCalculations(widget,def,functions[func]);
				newSeries.forEach(function(s){
					if (s) {
						seriesToAdd.push(s);
					}
				})
			}
		}

		//	Loop through all existing series, hide any that are marked
		widget.metadata.panel('values').items.forEach(function(item){

			//	Do we need to hide this series?
			if (item[hidePropName]){

				//	Loop through the existing series
				for (var j=widget.queryResult.series.length-1; j>=0; j--){

					//	Does the name attribute match the item's title?
					if (widget.queryResult.series[j].name === item.jaql.title){

						//	Yes it matches, remove from the array
						widget.queryResult.series.splice(j,1);
					}
				}
			}
		})

		//	Add in the new calculated series
		seriesToAdd.forEach(function(series){

			widget.queryResult.series.push(series);
		})
	}

	//////////////////////////////////
	//	Event Handlers				//
	//////////////////////////////////
	
	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		addHandler(args.widget, 'destroyed', onWidgetDestroyed);
		//	Should we run this?
		var shouldInit = widgetIsSupported(args.widget.type);
		//	Add hook for rendering
		if (shouldInit) {
			addHandler(args.widget, 'render', checkForFunctions);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", addMenuItem);

}]);