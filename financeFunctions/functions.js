
/*	Define financial functions as objects, then add to the array at bottom 	*/


//	Available functions array
var myFunctions = [];

//	Function for calculating internal rate of return
var rateOfReturn = {
	'label': 'Internal Rate of Return',
	'data': [
		{
			'label': 'Start Value'
		},{
			'label': 'Return Rate'
		}
	],
	'results': [
		{
			'label': 'Cumulative',
			'calculation': function(data, colorFunction){

				/* 	Business Logic:

				if (this starting value < 0) {
					1 - ((1 - Current Monthly Return) * (1 - Last Cumulative Return))
				} else {
					((Current Monthly Return + 1) * (Last Cumulative Return + 1)) - 1)
				}
				*/

				//	Init a data series to return
				var results = $.extend({}, data['Return Rate']);

				//	Init data objects
				var returnRate = data['Return Rate'].data,
					startValue = data['Start Value'].data,
					color = colorFunction(results.color,1);
				
				//	Loop through each data point
				var dataPoints = [];
				for (var i=0; i<returnRate.length; i++){

					//	Get the relavant data points
					var thisReturn = returnRate[i],
						thisStart = startValue[i],
						lastPoint = dataPoints[i-1];

					//	Create a new data point
					var newPoint = {
						'color': color,
						'marker': returnRate[i].marker,
						'queryResultIndex': returnRate[i].queryResultIndex,
						'selected': returnRate[i].selected,
						'selectionData': returnRate[i].selectionData
					}

					//	Which data point is this?
					if (i == 0){
						//	First point should be null
						newPoint.y = null;
					} else {
						//	All other points, try to calculate the CRR
						if (thisStart.y < 0){
							newPoint.y = 1 - ((1-thisReturn.y) * (1-lastPoint.y));
						} else{
							newPoint.y = ((thisReturn.y + 1) * (lastPoint.y + 1)) - 1;
						}
					}

					//	Save this point
					dataPoints.push(newPoint);
				}

				//	Update the results series
				results.data = dataPoints;
				results.color = color;
				results.name = 'Cumulative Rate of Return';

				//	Return the finished results
				return results;
			}
		},{
			'label': 'Annual',
			'calculation': function(data, colorFunction){

				/* 	Business Logic:

				Calculate the cumulative return first

				[(Absolute Value (1 + thisCumulativeReturn) ^ (12 / index) ) * SIGN(1 + thisCumulativeReturn)] - 1

				*/

				//	Init a data series to return
				var results = $.extend({}, data['Return Rate']);

				//	Init data objects
				var returnRate = data['Return Rate'].data,
					startValue = data['Start Value'].data,
					color = colorFunction(results.color,2);
				
				//	Loop through each data point to calculate the cumulative rate of return
				var cumulatives = [];
				for (var i=0; i<returnRate.length; i++){

					//	Get the relavant data points
					var thisReturn = returnRate[i],
						thisStart = startValue[i],
						lastPoint = cumulatives[i-1];

					//	Create a new data point
					var newPoint = null

					//	Which data point is this?
					if (i == 0){
						//	First point should be null
						newPoint = null;
					} else {
						//	All other points, try to calculate the CRR
						if (thisStart.y < 0){
							newPoint = 1 - ((1-thisReturn.y) * (1-lastPoint));
						} else{
							newPoint = ((thisReturn.y + 1) * (lastPoint + 1))  - 1; 
						}
					}

					//	Save this point
					cumulatives.push(newPoint);
				}

				//	Loop through each data point to calculate the annual rate of return
				var dataPoints = [];
				for (var i=0; i<returnRate.length; i++){

					//	Get the relavant data points
					var thisCumulativeReturn = cumulatives[i];

					//	Create a new data point
					var newPoint = {
						'color': color,
						'marker': returnRate[i].marker,
						'queryResultIndex': returnRate[i].queryResultIndex,
						'selected': returnRate[i].selected,
						'selectionData': returnRate[i].selectionData
					}

					//	Which data point is this?
					if (i <= 12){
						//	First point should be null
						newPoint.y = null;
					} else {
						//	All other points, try to calculate the CRR
						newPoint.y = Math.pow(Math.abs(1+thisCumulativeReturn), (12/i)) * Math.sign(1+thisCumulativeReturn) - 1;
					}

					//	Save this point
					dataPoints.push(newPoint);
				}

				//	Update the results series
				results.data = dataPoints;
				results.color = color;
				results.name = 'Annual Rate of Return';

				//	Return the finished results
				return results;
				
			}
		}
	]
}

//	Add to the functions list
myFunctions.push(rateOfReturn);